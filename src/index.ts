import Apprenants from "./Apprenant"
import Tickets from "./Tickets"

// Apprenants
const students = new Apprenants()
students.getAllApprenants()
students.postAnApprenant({username: "bobo",password: "mdp"})
students.listApprenants()

// Tickets
const tickets = new Tickets();
tickets.getAllValidTickets();
tickets.postTicket();
tickets.disableTicket();

