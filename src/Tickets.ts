export default class Tickets {

    /**
     * On récupère l'ensemble des tickets valides
     */
    getAllValidTickets() {
        const url = "https://web-help-request-api.herokuapp.com/tickets"

        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.tableauTickets()
            })
    }

    /**
     * On récupère le tableau qui possède tous les tickets
     */
    tableauTickets() {
        let tableau = document.getElementById("tableau_id");
        const tickets_url = `https://web-help-request-api.herokuapp.com/tickets`
        fetch(tickets_url).then((data) => {
            return data.json()
        }).then((api) => {
            api.data.forEach((element: any) => {
                tableau.innerHTML += `
                    <tr id="${element.id}">
                    <th scope="row">${element.id}</th>
                    <td>${element.users_id}</td>
                    <td>${element.subject}</td>
                    <td>${element.date}</td>
                    </tr> `
            })
        })
    }

    /**
     * On ajoute un nouveau ticket
     */
    postTicket() {
        document.getElementById("need_help").addEventListener("click", (event) => {
            event.preventDefault();
            //@ts-ignore
            let help = document.getElementById('subject').value;
            // @ts-ignore
            let select = document.getElementById("students_list_d").value
            let paramsObj = { subject: help, userId: select }
            let searchParams = new URLSearchParams(paramsObj);
            const url = "https://web-help-request-api.herokuapp.com/tickets"
            fetch(url, {
                method: 'POST',
                body: `${searchParams}`,
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
            })
                .then(response => response.json())
                .then(res => {
                    document.getElementById('subject').innerHTML = res.subject;
                })
                .catch(err => {
                    console.log('Error message: ', err);
                });
            window.location.reload()
        })
    }

    /**
     * Désactivation du premier ticket quand on clique sur le bouton "Au suivant !"
     */
    disableTicket() {
        document.getElementById("next_student").addEventListener("click", () => {
            // On récupère l'array des tickets
            fetch('https://web-help-request-api.herokuapp.com/tickets').then((response) =>
                response.json().then((data) => {
                    // Le ticket en question (dans notre cas, c'est le premier index)
                    fetch(`https://web-help-request-api.herokuapp.com/tickets/${data.data[0].id}`, {
                        method: 'PATCH'
                    })
                }))
        })
    }
}
