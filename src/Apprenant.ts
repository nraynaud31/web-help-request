export default class Apprenants {

    /**
     * On récupère tous les apprenant.e.s (pour l'ajout du ticket)
     */
    getAllApprenants() {
        const users_url = `https://web-help-request-api.herokuapp.com/users`
        fetch(users_url).then((data) => {
            return data.json()
        }).then((api) => {
            let options_students = document.getElementById("students_list_d")
            api.data.forEach((element: any) => {
                options_students.innerHTML += `<option value="${element.id}">${element.username}</option>`
            });
        })
    }

    /**
     * On liste les apprenant.e.s
     */
    listApprenants() {
        let list_group_ul = document.getElementById("list_students");
        const users_url = `https://web-help-request-api.herokuapp.com/users`
        fetch(users_url).then((data) => {
            return data.json()
        }).then((api) => {
            api.data.forEach((element: any) => {
                list_group_ul.innerHTML += `<li class="students_list list-group-item list-unstyled text-center">${element.id} ${element.username}</li>`
            });
        })
    }

    /**
     * On créé un apprenant
     * @param student Apprenant (username & string)
     */

    postAnApprenant(student: { username: string, password: string }) {
        let paramsStudent = { username: student.username, password: student.password };
        let searchParams = new URLSearchParams(paramsStudent);
        fetch("https://web-help-request-api.herokuapp.com/users", {
            // Adding method type
            method: "POST",
            // Adding body or contents to send
            body: `${searchParams}`,
            // Adding headers to the request
            headers: {
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            }
        }).then(response => response.json())
    }
}